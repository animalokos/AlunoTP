/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listaencadeada;

/**
 *
 * @author Binho
 */
public class ListaEncadeada <E> {

    private Node inicio;
    private Node fim;
    private Node pc;
    private int qtdElementos;

    public ListaEncadeada(){
        inicio       = null;
        fim          = null;
        pc           = null;
        qtdElementos = 0;
    }
    
    public void insere(E dado){
        Node nova  = new Node();
        nova.setValue(dado);
   
        if(qtdElementos == 0){
              inicio = nova;
              fim    = nova;
              pc     = nova;
              qtdElementos++;
        }else{
               fim.setNext(nova);
               fim         = nova;
               qtdElementos++;
        }        
    } 

    public void insere_pos(E dado, int pos){
        if(pos < 0 || pos > qtdElementos){
              System.out.println("posicao invalida \n");
        }else{
                Node nova  = new Node();
                nova.setValue(dado);
        
                for(int i=0; i<pos-1;i++){
                        pc = pc.getNext();
                }

                nova.setNext(pc.getNext());
                pc.setNext(nova);
                qtdElementos++;
        }   
    }
    
    public E remove_pos(int pos){
        E ret = null;
        if(qtdElementos == 0){
              System.out.println("Lista vazia \n");
        }else{
                if(pos < 0 || pos > qtdElementos){
                       System.out.println("posicao invalida \n");
                }else{
                        Node aux = new Node();
                        aux      = pc;
                       
                        aux = aux.getNext();
                     
                        for(int i=0; i<pos-1;i++){
                               pc  = pc.getNext();
                               aux = aux.getNext();
                        }
                      
                        ret = (E) aux.getValue();
                        pc.setNext(aux.getNext()); 
                        aux = null;                        
                        qtdElementos--;
                    }
        }
        return ret;
    }
    
    public void remove_elemento(E elemento){
        if(qtdElementos == 0){
            System.out.println("Lista vazia \n");
        }else{
                Node aux = new Node();
                aux      = pc;
                aux      = aux.getNext();
                
                for(int i=0; i<qtdElementos;i++){
                        if(pc.getValue() == elemento){
                            break;
                        }
                        aux      = aux.getNext();
                        pc       = pc.getNext();
                }
                
                if(pc == null){
                    System.out.println("Elemento nao encontrado \n");
                }else{
                        pc.setValue(aux.getNext());
                        aux = null;
                        qtdElementos--;
               }
        }
    }
    
           
    public int pesquisa_elemento(E elemento){
        int i, pos = 0;
        if(qtdElementos == 0){
             System.out.println("Lista vazia \n");
        }else{
                for(i=0; i<qtdElementos;i++){
                        if(pc.getValue() == elemento){
                            break;
                        }
                        pc = pc.getNext();
                        pos++;
                }
                if(pc == null){
                       return -1;
                }else{
                       return pos;
                }      
        }
        return pos;
    }
    
    public void voltarCurso(){
       pc = inicio; 
    }    
       
  
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
